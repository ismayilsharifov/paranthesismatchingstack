 
Steps to find whether a given expression is balanced or unbalanced


Input the expression and put it in a character stack.
Scan the characters from the expression one by one.
If the scanned character is a starting bracket ( ‘ ( ‘ or ‘ { ‘ or ‘ [ ‘), then push it to the stack.
If the scanned character is a closing bracket ( ‘ ) ’ or ‘ } ’ or ‘ ] ’ ), then pop from the stack and if the popped character is the equivalent starting bracket, then proceed. Else, the expression is unbalanced.
 After scanning all the characters from the expression, if there is any parenthesis found in the stack or if the stack is not empty, then the expression is unbalanced.
Now, let us see a program to check balanced parentheses in the given expression.


 (One of the possible syntax errors that compilers check for is the missing parentheses in code blocks.)