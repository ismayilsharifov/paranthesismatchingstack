#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#define MAX 110

struct Stack{
int pos;
char arr[MAX];
} s;

void init();
void push(char x);
void pop();
char top();
bool isFull();
bool isEmpty();
int size();
void cllear();
bool Pair(char op,char cl);

int main()
{
  char exp[MAX];
  int i;
  init();
  printf("Enter an expression:");
     scanf("%s",exp);
  int n=strlen(exp);
  for(i=0;i<n;i++){
      if(exp[i] == '(' || exp[i] == '{' || exp[i] == '[' || exp[i] == '<'){
              push(exp[i]);
      }else if(exp[i] == ')' || exp[i] == '}' || exp[i] == ']' || exp[i] == '>'){
        char a=top();
         printf("%c",a);
        if(isEmpty() || !Pair(top(),exp[i])){
            puts("false");
            return 0;
        }else{
            pop();
        }
    }
}
if(isEmpty()){
  puts("true");
}else{
  puts("false");
}
  return 0;
}


void init(){
    s.pos = -1;
}
void push(char x){
    if(isFull()){
        puts("false");
    }else {
        ++s.pos;
        s.arr[s.pos]=x;
    }
}
void pop(){
    if(isEmpty()){
        puts("true");
    }else {
        --s.pos;
    }
}
char top(){
    return s.arr[s.pos];
}
bool isFull(){
  if(s.pos == MAX-1){
      return true;
  }else{
      return false;
  }
}
bool isEmpty(){
  if(s.pos == -1){
      return true;
  }else{
      return false;
  }
}
bool Pair(char op,char cl)
{
	if(op == '(' && cl == ')') return true;
	else if(op == '{' && cl == '}') return true;
	else if(op == '[' && cl == ']') return true;
	else if(op == '<' && cl == '>') return true;
	return false;
}
